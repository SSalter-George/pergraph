test_that("Function arrow_set works as expected", {
  good_arrow_set <-
    arrow_set(
      "a", "f", "b",
      "a", "g", "c",
      "b", "f", "d",
      "a", "f", "b")
  expect_identical(
    good_arrow_set$from,
    c("a", "b", "a"))
  expect_identical(
    good_arrow_set$arrow,
    c("f", "f", "g"))
  expect_identical(
    good_arrow_set$to,
    c("b", "d", "c"))
  expect_identical(
    class(good_arrow_set),
    c("arrow_set", "tbl_df", "tbl", "data.frame")
  )
  expect_error(
    arrow_set(
      1, "f", "c",
      2, "g", "d"),
    stringr::str_c(
      "not compatible: " ,
      "- Incompatible types for column `from`: character vs double",
      sep = "\n"
    )
  )
  expect_error(
    arrow_set(
      "a","f", "c",
      "b", list("a", "b"), "d"),
    stringr::str_c(
      "not compatible: " ,
      "- Incompatible types for column `arrow`: character vs list",
      sep = "\n"
    )
  )
  expect_error(
    arrow_set(
      "a", "f", "c",
      "a", "g", c("a", "b")),
    stringr::str_c(
      "not compatible: " ,
      "- Incompatible types for column `to`: character vs list",
      sep = "\n"
    )
  )
  expect_error(
    arrow_set(
      "a", "f", "c",
      "a", "g")
  )
})


test_that("Functions union, intersect, setdiff and symdiff work as expected", {
  arrow_set_a <-
    arrow_set(
      "b", "h", "c",
      "c", "h", "d",
      "a", "f", "d")
  arrow_set_b <-
    arrow_set(
      "b", "h", "c",
      "c", "g", "e",
      "c", "h", "a")
  empty_arrow_set <- arrow_set()

  expect_identical(
    union(arrow_set_a, arrow_set_b),
    arrow_set(
      "a", "f", "d",
      "c", "g", "e",
      "b", "h", "c",
      "c", "h", "a",
      "c", "h", "d")
  )
  expect_identical(
    union(arrow_set_a, empty_arrow_set),
    arrow_set_a
  )
  expect_identical(
    union(empty_arrow_set, arrow_set_a),
    arrow_set_a
  )

  expect_identical(
    intersect(arrow_set_a, arrow_set_b),
    arrow_set(
      "b", "h", "c")
  )
  expect_identical(
    intersect(arrow_set_a, empty_arrow_set),
    empty_arrow_set
  )
  expect_identical(
    intersect(empty_arrow_set, arrow_set_a),
    empty_arrow_set
  )

  expect_identical(
    setdiff(arrow_set_a, arrow_set_b),
    arrow_set(
      "a", "f", "d",
      "c", "h", "d")
  )
  expect_identical(
    setdiff(arrow_set_a, empty_arrow_set),
    arrow_set_a
  )
  expect_identical(
    setdiff(empty_arrow_set, arrow_set_a),
    empty_arrow_set
  )

  expect_true(setequal(empty_arrow_set, empty_arrow_set))
  expect_true(setequal(arrow_set_a, arrow_set_a))

  expect_identical(
    symdiff(arrow_set_a, arrow_set_b),
    arrow_set(
      "a", "f", "d",
      "c", "g", "e",
      "c", "h", "a",
      "c", "h", "d",
      )
  )

  expect_identical(
    symdiff(arrow_set_a, empty_arrow_set),
    arrow_set_a
  )
  expect_identical(
    symdiff(empty_arrow_set, arrow_set_a),
    arrow_set_a
  )
})

